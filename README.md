Webmin
=========

A role to install webmin on ubuntu ! http://www.webmin.com/


Example Playbook
----------------

```yaml
- hosts: localhost
  roles:
     - webmin
```
